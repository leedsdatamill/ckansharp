﻿
namespace CkanSharp
{
    /// <summary>
    /// Options for uniqueness checking of abstract names
    /// when calling create methods
    /// </summary>
    public enum NameOptions
    {
        /// <summary>
        /// Do not check for uniqueness at all.
        /// (Not recommended)
        /// </summary>
        DoNotCheck,
        /// <summary>
        /// Check for uniqueness, and if name is not
        /// unique within the remote Ckan instance
        /// throw an exception saying as much.
        /// </summary>
        CheckOnly,
        /// <summary>
        /// Checks for uniqueness, and if name is not
        /// unique will add '-x' on the end of the name
        /// incrementing x by 1 until a unique name is
        /// returned.
        /// </summary>
        IncrementName,
        /// <summary>
        /// Will generate a GUID for the name field to
        /// guarantee uniqueness (Default).
        /// </summary>
        AutoName
    }
}