﻿using System.Net;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.Net.Http;

namespace CkanSharp.Abstracts
{
    /// <summary>
    /// Represents an abstraction of a User within Ckan
    /// </summary>
    public class User
    {
        private class JsonReadUser : JsonRead
        { public User Result { get; set; } }
        private class JsonReadUserList : JsonRead
        { public List<User> Result { get; set; } }

        public User() { }

        public string id { get; set; } //TODO make read only
        public string name { get; set; }
        public string apikey { get; set; } //TODO make read only
        public List<Organization> organizations { get; set; }
        public string display_name { get; set; }
        public string fullname { get; set; }

        public void Create(CkanCredentials credentials)
        {
            throw new System.NotImplementedException();
        }

        public static User Read(CkanCredentials credentials, string id)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization:" + credentials.ApiKey);

            string apiUrl = credentials.Host + ApiMethods.ReadUser + "?id=" + id;

            string jsonResponse = webClient.DownloadString(apiUrl);

            JsonReadUser jsonReader = new JsonReadUser();
            jsonReader = JsonConvert.DeserializeObject<JsonReadUser>(jsonResponse);
            jsonReader.checkSuccess();
            return jsonReader.Result;
        }

        public void Update(CkanCredentials credentials)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(CkanCredentials credentials)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Connects to the remote Ckan instance and 
        /// returns a list of User abstracts.
        /// </summary>
        /// <param name="credentials">The credentials to use
        /// when fetching the data.</param>
        /// <param name="query">OPTIONAL: The query text to filter on.</param>
        /// <returns>List of user abstracts.</returns>
        /// <remarks>Ckan credentials should be supplied when trying to
        /// establish the current user, along with the credentials.username
        /// as the query parameter.  This will return a list of Users
        /// which can then be checked against.  When an API key is found
        /// that matches the api Key in credentials, it is the
        /// User abstract relating to that set of credentials.</remarks>
        public static List<User> GetList(CkanCredentials credentials, string query = null)
        {
            WebClient webClient = new WebClient();
            if (credentials != null)
            {
                webClient.Headers.Add("Authorization:" + credentials.ApiKey);
            }
            
            string apiUrl = credentials.Host + ApiMethods.ListUser;
            if (query != null)
            {
                apiUrl += "?q=" + query;
            }
            string jsonResponse = webClient.DownloadString(apiUrl);

            JsonReadUserList jsonReader = new JsonReadUserList();
            jsonReader = JsonConvert.DeserializeObject<JsonReadUserList>(jsonResponse);
            jsonReader.checkSuccess();
            return jsonReader.Result;
        }

        

    }
}
