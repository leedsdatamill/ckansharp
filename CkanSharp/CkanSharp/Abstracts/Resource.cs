﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using RestSharp;

namespace CkanSharp.Abstracts
{
    /// <summary>
    /// Represents an abstraction of a Resource within CKAN
    /// </summary>
    public class Resource
    {

        private class JsonReadResource : JsonRead
        { public Resource Result { get; set; } }
        private class JsonReadResourceList : JsonRead
        { public List<Resource> Result { get; set; } }

        public Resource() {
            created = null;
            last_modified = null;
            cache_last_updated = null;
            webstore_last_updated = null;
        }

		//TODO: have a look at handling binary data upload via FileStore
		// http://ckan.readthedocs.org/en/latest/maintaining/filestore.html

        //TODO: have a think about reading the resource file back from ckan
        public string id { get; set; }
        public string package_id { get; set; }
        public string url { get; set; }
        public string revision_id { get; set; }
        public string description { get; set; }
        public string format { get; set; }
        public string hash { get; set; }
        //TODO: add a custom setter that validates name against: [a-z0-9-_]\w+
        public string name { get; set; }
        public string resource_type { get; set; }
        public string mimetype { get; set; }
        public string mimetype_inner { get; set; }
        public string webstore_url { get; set; }
        public string cache_url { get; set; }
        public Nullable<int> size { get; set; }
        public Nullable<DateTime> created { get; set; }
        public Nullable<DateTime> last_modified { get; set; }
        public Nullable<DateTime> cache_last_updated { get; set; }
        public Nullable<DateTime> webstore_last_updated { get; set; }


        public Resource Create(CkanCredentials credentials)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization:" + credentials.ApiKey);

            string apiUrl = credentials.Host + ApiMethods.CreateResource;
            string jsonResponse;

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            string jsonContent = JsonConvert.SerializeObject(this, settings);
            jsonResponse = webClient.UploadString(apiUrl, jsonContent);

            JsonReadResource jsonReader = new JsonReadResource();
            jsonReader = JsonConvert.DeserializeObject<JsonReadResource>(jsonResponse);
            jsonReader.checkSuccess();
            return jsonReader.Result;
        }

        public Resource Create(CkanCredentials credentials, byte[] fileBytes, 
                                int timeout, string fileName)
        {
            throw new NotImplementedException();
        }


    }
}
