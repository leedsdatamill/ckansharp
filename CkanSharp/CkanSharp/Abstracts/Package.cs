﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace CkanSharp.Abstracts
{
	/// <summary>
    /// Represents an abstraction of a Package within CKAN
    /// </summary>
    public class Package
    {

        private class JsonReadPackage : JsonRead
        { public Package Result { get; set; } }
        private class JsonReadStringList : JsonRead
        { public List<String> Result { get; set; } }
        /// <summary>
        /// This json container class is a bit more complex than others
        /// as Ckan api puts the Package array inside another json
        /// object instead of just Result: being an array of Package
        /// For response format check http://[ckansite]/api/3/package_search?q=[query]
        /// ------------------
        /// This could probably be handled better but done it this way
        /// for consistency with other Abstracts behaviour
        /// Chris Wilson 2014-04-16
        /// </summary>
        private class JsonReadPackageList : JsonRead
        {
            public class JsonPackageContainer
            {
                public int count { get; set; }
                public List<Package> results { get; set; }
            }
            public JsonPackageContainer result { get; set; }
        }

		//TODO: add a custom setter that validates name against: [a-z0-9-_]
        public string name { get; set; }
        public string title { get; set; }
        public string id { get; set; }
        public string author { get; set; }
        public string author_email { get; set; }
        public string maintainer { get; set; }
        public string maintainer_email { get; set; }
        public string license_id { get; set; }
        public string notes { get; set; }
        public string url { get; set; }
        //TODO: deal with checking version for no more
        //than 100 chars on property set.
        public string version { get; set; }
        public string state { get; set; }
        public string type { get; set; }
        public List<Resource> resources { get; set; }
        public string owner_org { get; set; }

        /// <summary>
        /// Creates a package in the remote Ckan instance 
        /// based on the value of populated properties and returns the
        /// newly created package abstract.
        /// </summary>
        /// <param name="credentials">The CkanCredentials to use for the request</param>
        /// <param name="nameOption">Whether or not to check for unique name
        /// property.  Default behaviour is to use a random GUID to
        /// guarantee uniquess.</param>
        /// <returns>Newly created package</returns>
        public Package Create(CkanCredentials credentials, 
                                NameOptions nameOption = NameOptions.AutoName)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization:" + credentials.ApiKey);

            //Checks for naming options:
            switch (nameOption)
            {
                case NameOptions.DoNotCheck:
                    //do nothing, proceed ahead in a reckless fashion!
                    break;
                case NameOptions.CheckOnly:
                    //name property should already be populated if using
                    //this option
                    if (name == null)
                    {
                        string exMessage = "Cannot use CheckOnly option without " +
                                            "something in the package name property";
                        throw new Exception(exMessage);
                    }
                    if (NameExists(credentials, name))
                    {
                        string exMessage = "Name is already in use";
                        throw new Exception(exMessage);
                    }
                    break;
                case NameOptions.IncrementName:
                    //name property should already be populated if using
                    //this option
                    if (name == null)
                    {
                        string exMessage = "Cannot use IncrementName option without " +
                                            "something in the package name property";
                        throw new Exception(exMessage);
                    }

                    /**
                     * TODO: this will take AGES if there is a few similarly
                     * named packages, and might upset the Ckan server.
                     * see notes by NameExists method below for details.
                     * need to fix.
                     * */
                    if (NameExists(credentials, name))
                    {
                        //start incrementing name
                        int nameIncrement = 1;
                        while (NameExists(credentials, name + "-" + nameIncrement))
                        {
                            nameIncrement++;
                        }
                        name += "-" + nameIncrement;
                    }
                    break;
                case NameOptions.AutoName:
                    name = Guid.NewGuid().ToString();
                    break;
            }

            string apiUrl = credentials.Host + ApiMethods.CreatePackage;
            string jsonResponse;

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            string jsonContent = JsonConvert.SerializeObject(this, settings);
            jsonResponse = webClient.UploadString(apiUrl, jsonContent);

            JsonReadPackage jsonReader = new JsonReadPackage();
            jsonReader = JsonConvert.DeserializeObject<JsonReadPackage>(jsonResponse);
            jsonReader.checkSuccess();
            return jsonReader.Result;
        }

        public static Package Read(CkanCredentials credentials, string id)
        {
            //TODO: note to self, ID can be Name or ID I think, should probably reflect
            //that in documentation comments.
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization:" + credentials.ApiKey);

            string apiUrl = credentials.Host + ApiMethods.ReadPackage + "?id=" + id;

            string jsonResponse = webClient.DownloadString(apiUrl);

            JsonReadPackage jsonReader = new JsonReadPackage();
            jsonReader = JsonConvert.DeserializeObject<JsonReadPackage>(jsonResponse);
            jsonReader.checkSuccess();
            return jsonReader.Result;
        }

        public static List<Package> List(CkanCredentials credentials, string query = null)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization:" + credentials.ApiKey);

            string apiUrl = credentials.Host;

            //different api method for list with or without query
            //this is inconsistent with behaviour of Organization_list
            //consider raising as issue with Ckan, but probably not.
            if (query != null)
            {
                apiUrl += ApiMethods.ListPackageSearch + "?q=" + query;
                string jsonResponse = webClient.DownloadString(apiUrl);
                JsonReadPackageList jsonReader = new JsonReadPackageList();
                jsonReader = JsonConvert.DeserializeObject<JsonReadPackageList>(jsonResponse);
                jsonReader.checkSuccess();
                return jsonReader.result.results;
            }
            else
            {
                //package_list only returns a string array, so
                //consume that array and then cycle it to retrieve
                //each package individually
                apiUrl += ApiMethods.ListPackage;
                string jsonResponse = webClient.DownloadString(apiUrl);
                JsonReadStringList jsonReader = new JsonReadStringList();
                jsonReader = JsonConvert.DeserializeObject<JsonReadStringList>(jsonResponse);
                jsonReader.checkSuccess();
                List<Package> tempPackages = new List<Package>();
                foreach (string packageName in jsonReader.Result)
                {
                    Package package = Package.Read(credentials, packageName);
                    tempPackages.Add(package);
                }
                return tempPackages;
            }
        }

        public static List<string> ListNames(CkanCredentials credentials)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization:" + credentials.ApiKey);

            string apiUrl = credentials.Host + ApiMethods.ListPackage;

            string jsonResponse = webClient.DownloadString(apiUrl);
            JsonReadStringList jsonReader = new JsonReadStringList();
            jsonReader = JsonConvert.DeserializeObject<JsonReadStringList>(jsonResponse);
            jsonReader.checkSuccess();
            return jsonReader.Result;
        }

        /// <summary>
        /// Helper to establish whether a package name already exists in the remote
        /// Ckan instance.  Since package_search (accessed in CkanSharp 
        /// by calling Package.List with a query parameter) doesn't return private
        /// packages, but package_list does, we have to iterate every package!
        /// not ideal as I've written Package.List specifically to call back for each 
        /// string received and fully read the package into a Package abstract.
        /// hence uses the Package.ListNames method to only get the list of strings.
        /// TODO: fix this chris, neaten it all up.
        /// </summary>
        /// <param name="credentials"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool NameExists(CkanCredentials credentials, string name)
        {
            List<string> packageNames = Package.ListNames(credentials);
            foreach (string packageName in packageNames)
            {
                if (packageName == name)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
