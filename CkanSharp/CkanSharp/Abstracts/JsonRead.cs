﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CkanSharp.Abstracts
{
    /// <summary>
    /// Provides a basic structure to map Ckan JSON responses to.
    /// This class should be inherited as a nested private class
    /// within each abstract, with a property added for 
    /// Result:[AbstractType]
    /// </summary>
    internal class JsonRead
    {

        public JsonRead() { }

        public string Help { get; set; }
        public string Error { get; set; }
        public bool Success { get; set; }


        /// <summary>
        /// Checks the success of the json response, and if failure
        /// is found throws an exception appropriately.
        /// </summary>
        public void checkSuccess()
        {

            if (Success != true)
            {
                if (Error != null && Help != null)
                {
                    throw new Exception("An error has occured: " + Error +
                                        "\n Help information: " + Help);
                }
                else
                {
                    throw new Exception("An unknown error has occured processing the " +
                                        "response from the server.  Sorry :(");
                }
            }

        }
    }
}
