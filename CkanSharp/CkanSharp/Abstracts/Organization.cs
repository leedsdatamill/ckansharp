﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System;
using System.Net.Http;

namespace CkanSharp.Abstracts
{
    /// <summary>
    /// Represents an abstraction of an Organization within CKAN
    /// </summary>
    public class Organization
    {
        /// <summary>
        /// Class acts as a simple container to deal with Json
        /// response ambiguity from Ckan.  See more detailed comment
        /// within method GetList().
        /// </summary>
        private class OrganizationUser
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }

        private class JsonReadOrganization : JsonRead
        { public Organization Result { get; set; } }
        private class JsonReadOrganizationList : JsonRead
        { public List<OrganizationUser> Result { get; set; } }
        private class JsonReadStringList : JsonRead
        { public List<string> Result { get; set; } }

        //TODO: flesh out in line with CKAN properties
        public string Id { get; set; }
        public string Name { get; set; }
        public string Display_Name { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string State { get; set; }
        public DateTime Created { get; set; }
        public List<Package> Packages { get; set; }
        public List<User> Users { get; set; }

        public void Create(CkanCredentials credentials)
        {
            //TODO: put a check in here to make sure that sub
            //dicts aren't sent too (Packages, Users)
            throw new System.NotImplementedException();
        }

        public static Organization Read(CkanCredentials credentials, string name)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization:" + credentials.ApiKey);
            string apiUrl = credentials.Host + ApiMethods.ReadOrganization + "?id=" + name;
            string jsonResponse = webClient.DownloadString(apiUrl);

            JsonReadOrganization jsonReader = new JsonReadOrganization();
            jsonReader = JsonConvert.DeserializeObject<JsonReadOrganization>(jsonResponse);
            jsonReader.checkSuccess();
            return jsonReader.Result;
        }

        public void Update(CkanCredentials credentials)
        {
            //TODO: put a check in here to make sure that sub
            //dicts aren't sent too (Packages, Users)
            throw new System.NotImplementedException();
        }

        public void Delete(CkanCredentials credentials)
        {
            throw new System.NotImplementedException();
        }


        public static List<Organization> GetList(CkanCredentials credentials, 
                                                    List<string> query = null)
        {

            /**
             * The Organization list & show methods have ambiguous return values
             * if all_fields is set to true... this will cause problems with
             * reflection based json de-serialization.
             * **********************************************
             * In _List - "packages" references an integer representing the package
             * count for the organization.
             * in _Show - "packages" references an array of dicts representing
             * the actual package objects for that organization.
             * **********************************************
             * Therefore behaviour of this method is out of line with rest of the library
             * it initially just gets a list of strings representing the names
             * and then subsequently reads each organization individually before
             * returning the new List of Orgs.  Will impact performance significantly
             * due to high number of individual api calls.
             * Chris Wilson - 2014/04/11
             */

            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization:" + credentials.ApiKey);

            string apiUrl = credentials.Host + ApiMethods.ListOrganization;
            string jsonResponse;
            if (query != null)
            {
                List<string> organizations = query;
                string jsonContent = JsonConvert.SerializeObject(organizations);
                jsonResponse = webClient.UploadString(apiUrl, jsonContent);
            }
            else
            {
                jsonResponse = webClient.DownloadString(apiUrl);
            }
            
            JsonReadStringList jsonReader = new JsonReadStringList();
            jsonReader = JsonConvert.DeserializeObject<JsonReadStringList>(jsonResponse);
            jsonReader.checkSuccess();

            List<Organization> tempOrgs = new List<Organization>();
            foreach (string orgName in jsonReader.Result)
            {
                tempOrgs.Add(Organization.Read(credentials, orgName));
            }

            
            return tempOrgs;
        }

        public static List<Organization> GetUsersList(CkanCredentials credentials)
        {
            //Same issue with json dict ambiguity as with GetList() above!

            WebClient webClient = new WebClient();
            webClient.Headers.Add("Authorization:" + credentials.ApiKey);

            string apiUrl = credentials.Host + ApiMethods.ListUserOrganizations;
            string jsonResponse;
            jsonResponse = webClient.DownloadString(apiUrl);

            JsonReadOrganizationList jsonReader = new JsonReadOrganizationList();
            jsonReader = JsonConvert.DeserializeObject<JsonReadOrganizationList>(jsonResponse);
            jsonReader.checkSuccess();

            List<Organization> tempOrgs = new List<Organization>();
            foreach (OrganizationUser org in jsonReader.Result)
            {
                tempOrgs.Add(Organization.Read(credentials, org.Name));
            }

            return tempOrgs;
        }
    }
}
