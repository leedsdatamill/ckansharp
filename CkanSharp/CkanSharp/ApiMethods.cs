﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CkanSharp
{
    internal static class ApiMethods
    {
        #region CREATE
        public const string CreateUser = "api/3/action/user_create";
        public const string CreatePackage = "api/3/action/package_create";
        public const string CreateOrganization = "api/3/action/organization_create";
        public const string CreateResource = "api/3/action/resource_create";
        #endregion

        #region READ
        public const string ReadUser = "api/3/action/user_show";
        public const string ReadOrganization = "api/3/action/organization_show";
        public const string ReadPackage = "api/3/action/package_show";
        #endregion

        #region UPDATE
        public const string UpdateResource = "api/3/action/resource_update";


        #endregion

        #region DELETE
        //DELETE methods go here

        #endregion

        #region LIST
        public const string ListUser = "api/3/action/user_list";
        public const string ListOrganization = "api/3/action/organization_list";
        public const string ListUserOrganizations = "api/3/action/organization_list_for_user";
        public const string ListPackage = "api/3/action/package_list";
        public const string ListPackageSearch = "api/3/action/package_search";
        #endregion
    }
}
