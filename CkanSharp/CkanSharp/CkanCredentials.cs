﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CkanSharp.Abstracts;
using System.Net;

namespace CkanSharp
{
    /// <summary>
    /// Class to contain the Authorization credentials for communicating with
    /// a named remote Ckan instance.
    /// </summary>
    public class CkanCredentials
    {
       
        /// <summary>
        /// Initializes an instance of Ckan Credentials based on
        /// the provided Host name and Api Key.  Makes a call to the
        /// remote instance and retrieves details of the current
        /// user into the CurrentUser property.
        /// </summary>
        /// <param name="host">The Host name of the remote Ckan instance
        /// as a URL.  e.g. "http://myckan.org.uk/"</param>
        /// <param name="apiKey">The Api Key to use for Authorization
        /// when communicating with the remote Ckan instance</param>
        /// <param name="username">The username to use with the ApiKey</param>
        public CkanCredentials(string host, string apiKey, string username)
        {
            if (host.Substring(host.Length - 1, 1) != "/")
            {
                host += "/";
            }

            _Host = host;
            _ApiKey = apiKey;
            _Username = username;
            _CurrentUser = GetCurrentUser();
        }

        private string _Host;
        private string _ApiKey;
        private string _Username;
        private User _CurrentUser;

        /// <summary>
        /// READ ONLY: Gets the remote Ckan instance Host name.
        /// </summary>
        public string Host { get { return _Host; } }
        /// <summary>
        /// READ ONLY: Gets the remote Ckan instance Api Key to use for Authorization.
        /// </summary>
        public string ApiKey { get { return _ApiKey; } }
        /// <summary>
        /// READ ONLY: Gets the username being used for the remote Ckan instance.
        /// </summary>
        public string Username { get { return _Username; } }
        /// <summary>
        /// READ ONLY: Gets the current User for the remote Ckan instance as 
        /// a User Abstract.
        /// </summary>
        public User CurrentUser { get { return _CurrentUser; } }

        /// <summary>
        /// Returns a new User abstract based on the Ckan Credentials
        /// that represents the current user.
        /// </summary>
        /// <returns>User abstract representing the current user</returns>
        private User GetCurrentUser()
        {
            List<User> Users = User.GetList(this, Username);
            foreach (User user in Users)
            {
                if (user.apikey != null && user.apikey == ApiKey)
                {
                    return user;
                }
            }
            throw new Exception("Ckan credentials could not be resolved to a User");
        }

    }
}
