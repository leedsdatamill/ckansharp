## Welcome to CkanSharp

CkanSharp (also ckan#) aims to eventually be a simple abstraction
of the Ckan api based on an object orientated approach.

The result will be a .Net assembly that allows you to work with 
a ckan remote instance using .Net objects that expose basic
methods, with all of the api communication handled by ckan#.

ckan# is a _work in progress_ so expect large bulky commits 
for a while.  Any suggestions for development, bugs or other 
issues can be added to the Issues log.

### Intended uses
ckan# is intended to be integrated into a large organisation's 
internal network to help facilitate remote management of their 
openly published datasets.  Primary target is currently Local 
Authorities.

Current plans for development include integration into a wider 
Windows service that can be configured to have scheduled "jobs" 
that will update the requisite ckan package.